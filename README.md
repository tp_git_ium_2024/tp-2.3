# TP 2.3: Tester git diff

## 1- Reprendre le TP2.2 précédent dans son état final (MonAnniv3) et lister les versions avec ``git log``

![Q1](Screenshots/TP2.3 Q1.png)

## 2- Afficher la différence entre la version courante et la 1ère version enregistrée, entre la version courante et la 2ème version enregistrée, et entre La 1ere version et la 2ème version
![Q2](Screenshots/TP2.3 Q2.png)

## 3- Modifiez un fichier du répertoire de travail et afficher, avec ``git diff`` les différences introduites
![Q3](Screenshots/TP2.3 Q3.png)